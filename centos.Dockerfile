FROM centos:7

WORKDIR /
RUN yum update -y && \
    yum install -y centos-release-scl && \
    yum -y install \
        devtoolset-10-gcc devtoolset-10-gcc-c++ \ 
        make autoconf \
        zip unzip \
        file which \
        diffutils \
        findutils \
        libXtst-devel libXt-devel libXrender-devel libXrandr-devel libXi-devel \
        cups-devel \
        fontconfig-devel \
        alsa-lib-devel \
        git \
        wget && \
    yum clean all && \
    git clone -b release-1.8.1 https://github.com/google/googletest && \
    git clone https://github.com/openjdk/jtreg.git

RUN wget --progress=dot:mega -O openjdk17.tar.gz \
        https://download.java.net/java/GA/jdk17.0.2/dfd4a8d0985749f896bed50d7138ee7f/8/GPL/openjdk-17.0.2_linux-x64_bin.tar.gz && \
    tar -xzf openjdk17.tar.gz && rm -f openjdk17.tar.gz

WORKDIR /jtreg
SHELL ["scl", "enable", "devtoolset-10"]
RUN bash ./make/build.sh --jdk /jdk-17.0.2/

ENTRYPOINT ["scl", "enable", "devtoolset-10", "--"]

# WORKDIR /jdk17
# COPY  . .
# RUN bash configure \
#         --with-jtreg='/jtreg/build/images/jtreg' \
#         --with-gtest='/googletest/' \
#         --with-boot-jdk='/jdk-17.0.2/' && \
#     make images
